import ez_setup
ez_setup.use_setuptools()
from setuptools import setup, find_packages
import sys

#NOTE: if you want to develop askbot
#you might want to install django-debug-toolbar as well

setup(
    name="knowledgepoint",
    version='0.0.1',
    description='Knowledgepoint extensions for Askbot',
    packages=find_packages(),
    author='Evgeny.Fadeev',
    author_email='evgeny.fadeev@gmail.com',
    license='GPLv3',
    keywords='forum, community, wiki, Q&A',
    url='http://knowledgepoint.org',
    include_package_data = True,
    install_requires = ('askbot'),
)
