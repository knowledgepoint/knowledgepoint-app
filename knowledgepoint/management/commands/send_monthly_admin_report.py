from collections import OrderedDict
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import activate

from optparse import make_option
from templated_email import send_templated_mail

from ...reports import (
    AnswersAndCommentsByGroupReport,
    GroupMembershipReport,
    MembersInterestedInAreaReport,
    MembersInCountryReport,
    QuestionsAnsweredFromOtherSitesReport,
    QuestionsAskedByGroupReport,
    QuestionsGroupedByTagReport,
    QuestionsGroupedByCountryReport,
    QuestionsMovedToPublicDomainReport,
    UnansweredQuestionsByGroupReport,
)


MEMBERS_TAG_NAME = 'members of the {0} group'
MEMBERS_BY_TAG_OF_INTEREST_NAME = 'number of times tags of interest are followed by members of the {0} group'
MEMBERS_BY_COUNTRY_NAME = 'members of the {0} group, by countries'
QUESTIONS_FROM_GROUP_NAME = 'questions asked to the {0} group'
QUESTIONS_BY_TAG_OF_INTEREST_NAME = 'questions asked to the {0} group, by tag'
QUESTIONS_BY_COUNTRY_NAME = 'questions asked to the {0} group, by enquirer\'s country'
ANSWERS_AND_COMMENTS_BY_GROUP_NAME = 'answers and comments from the {0} group'
UNANSWERED_QUESTIONS_BY_GROUP_NAME = 'unanswered questions shared with the {0} group'
QUESTIONS_MOVED_TO_PUBLIC_DOMAIN_NAME = 'questions moved to public domain from the {0} group'
ANSWERED_QUESTIONS_FROM_PUBLIC_DOMAIN_NAME = 'questions answered by {0} members from public domain'


REPORT_TAGS = [
    MEMBERS_TAG_NAME,
    MEMBERS_BY_TAG_OF_INTEREST_NAME,
    MEMBERS_BY_COUNTRY_NAME,
    QUESTIONS_FROM_GROUP_NAME,
    QUESTIONS_BY_TAG_OF_INTEREST_NAME,
    QUESTIONS_BY_COUNTRY_NAME,
    ANSWERS_AND_COMMENTS_BY_GROUP_NAME,
    UNANSWERED_QUESTIONS_BY_GROUP_NAME,
    QUESTIONS_MOVED_TO_PUBLIC_DOMAIN_NAME,
    ANSWERED_QUESTIONS_FROM_PUBLIC_DOMAIN_NAME
]


REPORTS_FOR_TAGS = {
    MEMBERS_TAG_NAME: GroupMembershipReport,
    MEMBERS_BY_TAG_OF_INTEREST_NAME: MembersInterestedInAreaReport,
    MEMBERS_BY_COUNTRY_NAME: MembersInCountryReport,
    QUESTIONS_FROM_GROUP_NAME: QuestionsAskedByGroupReport,
    QUESTIONS_BY_TAG_OF_INTEREST_NAME: QuestionsGroupedByTagReport,
    QUESTIONS_BY_COUNTRY_NAME: QuestionsGroupedByCountryReport,
    ANSWERS_AND_COMMENTS_BY_GROUP_NAME: AnswersAndCommentsByGroupReport,
    UNANSWERED_QUESTIONS_BY_GROUP_NAME: UnansweredQuestionsByGroupReport,
    QUESTIONS_MOVED_TO_PUBLIC_DOMAIN_NAME: QuestionsMovedToPublicDomainReport,
    ANSWERED_QUESTIONS_FROM_PUBLIC_DOMAIN_NAME: QuestionsAnsweredFromOtherSitesReport
}


def format_report_tag(tag, group):
    return tag.format(group.name)


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--groups',
            dest='group_names',
            default=False,
            help='Specify the groups to send emails to.'),
        )
    help = (
        "This command sends out emails containing data about the number of "
        "members in a group, the tags members of a group have marked as being "
        "of interest, along with how many members are interested in it and "
        "the numbers of members belonging to different countries according to "
        "their profile data. It needs a list of group names, separated by "
        "commas, to know which groups to get the data for. These should be "
        "provided with the --groups option, so the command would look "
        "something like:\n\n./manage.py send_monthly_admin_report "
        "--groups=\"Group Name\"\n\nor\n\n./manage.py "
        "send_monthly_admin_report --groups=\"Group1,Group2\""
    )

    group_names = None

    def add_report_data(self, group, group_data, tag, report_class):
        report = report_class(group.name)
        group_data[group.name][tag].update(report.generate_data())

    @property
    def groups_list(self):
        return Group.objects.filter(name__in=self.group_names)

    def get_report_context_data(self, group):
        """
        The data structure here is actually fairly complex.
        The main dictionary contains a list of group reports.
        Each report is stored in a dictionary using the group name as a key, with 3 inner dictionaries storing the actual data.
        The data itself is also a dictionary so the end result looks something like this:

        {
            'group_reports: {
                    'Group Name': {
                        'members': {'Number of members': 0},
                        'tags_of_interest: {'Waste': 1, 'Agriculture': 2},
                        'countries: {'United Kingdom': 3, 'USA': 4}
                    }
                }
        }
        """
        context_data = {'group_reports': {group.name: OrderedDict()}}
        reports_dict = context_data['group_reports']
        for key in REPORT_TAGS:
            key_name = format_report_tag(key, group)
            reports_dict[group.name][key_name] = OrderedDict()
            report = REPORTS_FOR_TAGS[key]
            self.add_report_data(group, reports_dict, key_name, report)

        return context_data

    def handle(self, *args, **options):
        activate(settings.LANGUAGE_CODE)

        if 'group_names' not in options or not options['group_names']:
            raise CommandError('You must specify which groups you wish to send reports to.')

        self.group_names = options['group_names'].split(',')
        groups = self.groups_list
        if not groups:
            self.stdout.write('No matching Group objects found\n')
        else:
            from_email = settings.KP_FROM_EMAIL
            recipient_list = [admin[1] for admin in settings.KP_GROUP_ADMINS]
            for group in groups:
                self.stdout.write("Processing report for Group '%s'\n" % group.name)
                report_context = self.get_report_context_data(group)
                send_templated_mail(
                    template_name='reports_email',
                    from_email=from_email,
                    recipient_list=recipient_list,
                    context=report_context,
                    # Optional:
                    # cc=['cc@example.com'],
                    # bcc=['bcc@example.com'],
                    # headers={'My-Custom-Header':'Custom Value'},
                    template_prefix="email/",
                    template_suffix="email",
                )
                self.stdout.write(
                    "Report email sent for Group '%s' from '%s' to '%s'.\n" % (
                        group.name,
                        from_email,
                        recipient_list
                    )
                )
