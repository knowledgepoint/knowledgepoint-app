"""Forms for the Knowledgepoint extension"""
from django import forms
from askbot import forms as askbot_forms
from knowledgepoint.models import ThreadData

class AskForm(askbot_forms.AskForm):
    country = askbot_forms.CountryField(required=False)

class EditQuestionForm(askbot_forms.EditQuestionForm):
    country = askbot_forms.CountryField(required=False)

    def __init__(self, *args, **kwargs):
        """custom init to populate the inital data"""
        if 'question' in kwargs and 'initial' in kwargs:
            question = kwargs['question']
            if question.id:
                try:
                    data = ThreadData.objects.get(thread=question.thread)
                    kwargs['initial']['country'] = data.country
                except:
                    pass
        super(EditQuestionForm, self).__init__(*args, **kwargs)
        self.fields['suppress_email'].initial = True

class EditAnswerForm(askbot_forms.EditAnswerForm):
    def __init__(self, *args, **kwargs):
        super(EditAnswerForm, self).__init__(*args, **kwargs)
        self.fields['suppress_email'].initial = True
