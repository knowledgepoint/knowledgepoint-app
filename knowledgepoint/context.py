from knowledgepoint.models import ThreadData
from django.contrib.sites.models import Site

def common(request):
    if request.user.is_authenticated():
        profile = request.user.askbot_profile
        site = Site.objects.get_current()
        return {
            'subscribed_for_site': profile.is_subscribed_for_site(site),
            'site_id': site.id
        }
    return dict()
    

def question_page(request, data):
    try:
        thread_data = ThreadData.objects.get(thread=data['thread'])
        context = {'country': thread_data.country}
        return context
    except:
        return {}
        
    
